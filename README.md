# Репозиторий, куда вынесен gitlab-ci для деплоя приложения

### Rules:
```
.dev_rules: &dev-rules
  - if: $CI_COMMIT_BRANCH == "dev"
    when: manual
  - when: never

.feature_rules: &feature-rules
  - if: $CI_COMMIT_REF_NAME =~ /^feature/
    when: manual
  - when: never

.prod_rules: &prod-rules
  - if: $CI_COMMIT_TAG
    when: manual
  - when: never
```

### Environments:
```
.deploy_dev_env: &deploy-dev-env
  name: todoapp-dev
  url: https://dev.va1erify.ru

.deploy_feature_env: &deploy-feature-env
  name: todoapp-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHORT_SHA}
  url: https://${CI_COMMIT_SHORT_SHA}.dev.va1erify.ru

.deploy_prod_env: &deploy-prod-env
  name: todoapp-prod
  url: https://prod.va1erify.ru
```

* Из dev ветки push образа только в gitlab container registry и деплой только на dev контур с созданием "https://dev.va1erify.ru" environment 
* Из веток начинающихся с "feature" push образа только в gitlab container registry и деплой только на dev контур с созданием "https://hashcommit.dev.va1erify.ru" environment
* Деплой на прод только при наличии тега. Push образа в gitlab container registry и в docker hub registry. Создание "https://prod.va1erify.ru" environment